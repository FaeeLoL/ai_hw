import random
from constants import *
major = [0, 2, 4, 5, 7, 9, 11]
minor = [0, 2, 3, 5, 7, 8, 10]
accords_start = [0, 3, 4]


class Component:
    notes = None
    velocity = None
    fitness = None
    local_max_fitness = None
    local_max_notes = None
    note_pull = None

    def __init__(self, note_pull):
        self.note_pull = note_pull
        self.notes = [random.choice(note_pull) for i in range(48)]
        self.local_max_notes = self.notes.copy()
        self.velocity = [0] * 48
        self.fitness = self.fitness_function()
        self.local_max_fitness = self.fitness

    def evolution(self, global_max):
        for i in range(48):
            self.velocity[i] = W * random.uniform(0, 1) * self.velocity[i] + \
                                 C1 * random.uniform(0, 1) * (self.local_max_notes[i] - self.notes[i]) + \
                                 C2 * random.uniform(0, 1) * (global_max[i] - self.notes[i])
            self.notes[i] = self.notes[i] + self.velocity[i]
        self.fitness = self.fitness_function()
        if self.local_max_fitness > self.fitness:
            self.local_max_fitness = self.fitness
            self.local_max_notes = self.notes

    def in_range(self, x):
        return (x > 48) and (x < 72)

    def fitness_function(self):
        result = INF
        # for i in range(12):
        #     for maj in range(2):
        #         tone = []
        #         if maj == 0:
        #             for j in major:
        #                 tone.append((i + j) % 12)
        #         else:
        #             for j in minor:
        #                 tone.append((i + j) % 12)
        tone = [0, 2, 4, 5, 7, 9, 11]
        local_result = 0

        for acc0, acc1, acc2 in zip(self.notes[0::3], self.notes[1::3], self.notes[2::3]):
            local_local_result = INF
            acc0, acc1, acc2 = sorted([acc0, acc1, acc2])
            for a_s in accords_start:
                pre_result = abs(acc0 % 12 - tone[a_s]) + \
                             abs(acc1 % 12 - tone[(a_s + 2) % 7]) + \
                             abs(acc2 % 12 - tone[(a_s + 4) % 7])
                local_local_result = min(pre_result, local_local_result)

                if abs(acc2 - acc0) > 12:
                    local_local_result += 100 * (acc2 - acc0 - 12)

            local_result += local_local_result
        # for note in self.notes:
        #     pre_result = INF
        #     for check_note in self.note_pull:
        #         pre_result = min(pre_result, abs(check_note - note) * 10)
        #     local_result += pre_result
        # sorted_last = sorted([self.notes[45], self.notes[46], self.notes[47]])
        # local_result += 10 * (abs(sorted_last[0] % 12 - tone[0]) +
        #                       abs(sorted_last[1] - sorted_last[0] - tone[2]) +
        #                       abs(sorted_last[2] - sorted_last[0] - tone[4]))
        #
        # for z in range(3, 48, 3):
        #     ch1 = min(self.notes[z - 3], self.notes[z - 2], self.notes[z - 1])
        #     ch2 = min(self.notes[z], self.notes[z + 1], self.notes[z + 2])
        #     if abs(ch1 - ch2) > 6:
        #         local_result += 50 * (abs(ch1 - ch2) - 6)
        #     ch1 = max(self.notes[z - 3], self.notes[z - 2], self.notes[z - 1])
        #     ch2 = max(self.notes[z], self.notes[z + 1], self.notes[z + 2])
        #     if abs(ch1 - ch2) > 6:
        #         local_result += 50 * (abs(ch1 - ch2) - 6)
        result = min(result, local_result)

        for i in self.notes:
            if not self.in_range(i):
                result += 1000

        return result

    def print_all_information(self):
        print("notes =", self.notes)
        print("velocity =", self.velocity)
        print("fitness =", self.fitness)
        print("local_max_notes =", self.local_max_notes)
        print("local_max_fitness =", self.local_max_fitness)
