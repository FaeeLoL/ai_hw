from note import Component
from constants import *
import midi
import threading
global_fitness_max = INF
global_max_notes = []
notes = [48, 50, 52, 53, 55, 57, 59, 60, 62, 64, 65, 67, 69, 71]


def print_logs(it):
        print("iteration №", it)
        print("global_fitness =", global_fitness_max)
        if it % 20 == 0:
            print("global_notes =", global_max_notes)


a = [Component(notes) for i in range(population)]

for i in range(population):
    if global_fitness_max > a[i].local_max_fitness:
        global_fitness_max = a[i].local_max_fitness
        global_max_notes = a[i].local_max_notes.copy()

for iteration in range(iterations):
    for i in range(population):
        a[i].evolution(global_max_notes)
        if global_fitness_max > a[i].local_max_fitness:
            global_fitness_max = a[i].local_max_fitness
            global_max_notes = a[i].local_max_notes.copy()
    W *= 0.99
    print_logs(iteration)

for i in range(48):
    global_max_notes[i] = round(global_max_notes[i])
for i, j, z, in zip(global_max_notes[0::3], global_max_notes[1::3], global_max_notes[2::3]):
    print(sorted([i, j, z]))
midi.get_midi(global_max_notes)
