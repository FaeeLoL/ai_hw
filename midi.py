from midiutil import MIDIFile


def get_midi(degrees):
    track = 0
    channel = 0
    time = 0    # In beats
    duration = 1    # In beats
    tempo = 180   # In BPM
    volume = 100  # 0-127, as per the MIDI standard

    my_midi = MIDIFile(1)  # One track, defaults to format 1 (tempo track is created automatically)
    my_midi.addTempo(track, time, tempo)

    for i in range(0, len(degrees), 3):
        my_midi.addNote(track, channel, degrees[i], time + i / 2, duration, volume)
        my_midi.addNote(track, channel, degrees[i + 1], time + i / 2, duration, volume)
        my_midi.addNote(track, channel, degrees[i + 2], time + i / 2, duration, volume)

    with open("major-scale.mid", "wb") as output_file:
        my_midi.writeFile(output_file)
